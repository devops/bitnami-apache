ARG image_tag="latest"

FROM bitnami/apache:${image_tag}

USER 0

RUN install_packages curl libapache2-mod-auth-openidc libapache2-mod-xsendfile

USER 1001
